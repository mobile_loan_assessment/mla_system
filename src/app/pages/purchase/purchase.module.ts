import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PrimeModule} from '../../prime/prime.module';
import {PurhcaseComponent} from './purhcase.component';



const routes: Routes = [
    {
        path: '',
        component: PurhcaseComponent
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes), PrimeModule],
    declarations: [PurhcaseComponent]
})
export class PurchaseModule { }
