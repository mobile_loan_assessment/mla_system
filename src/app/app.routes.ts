import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
// import { DashboardDemoComponent } from './demo/view/dashboarddemo.component';
// import { SampleDemoComponent } from './demo/view/sampledemo.component';
// import { FormsDemoComponent } from './demo/view/formsdemo.component';
// import { DataDemoComponent } from './demo/view/datademo.component';
// import { PanelsDemoComponent } from './demo/view/panelsdemo.component';
// import { OverlaysDemoComponent } from './demo/view/overlaysdemo.component';
// import { MenusDemoComponent } from './demo/view/menusdemo.component';
// import { MessagesDemoComponent } from './demo/view/messagesdemo.component';
// import { MiscDemoComponent } from './demo/view/miscdemo.component';
// import { EmptyDemoComponent } from './demo/view/emptydemo.component';
// import { ChartsDemoComponent } from './demo/view/chartsdemo.component';
// import { FileDemoComponent } from './demo/view/filedemo.component';
// import { DocumentationComponent } from './demo/view/documentation.component';


// export const routes: Routes = [
//     { path: '', component: DashboardDemoComponent },
//     { path: 'components/sample', component: SampleDemoComponent },
//     { path: 'components/forms', component: FormsDemoComponent },
//     { path: 'components/data', component: DataDemoComponent },
//     { path: 'components/panels', component: PanelsDemoComponent },
//     { path: 'components/overlays', component: OverlaysDemoComponent },
//     { path: 'components/menus', component: MenusDemoComponent },
//     { path: 'components/messages', component: MessagesDemoComponent },
//     { path: 'components/misc', component: MiscDemoComponent },
//     { path: 'pages/empty', component: EmptyDemoComponent },
//     { path: 'components/charts', component: ChartsDemoComponent },
//     { path: 'components/file', component: FileDemoComponent },
//     { path: 'documentation', component: DocumentationComponent }
// ];

export const routes: Routes = [
    { path: '', loadChildren: () => import('./demo/view/dashboarddemo.module').then(m => m.DashboardDemoModule) },
    { path: 'components/sample', loadChildren: () => import('./demo/view/sampledemo.module').then(m => m.SampleDemoModule) },
    { path: 'components/purchase', loadChildren: () => import('./pages/purchase/purchase.module').then(m => m.PurchaseModule) },
    { path: 'components/forms', loadChildren: () => import('./demo/view/formsdemo.module').then(m => m.FormsDemoModule) },
    { path: 'components/data', loadChildren: () => import('./demo/view/datademo.module').then(m => m.DataDemoModule) },
    { path: 'components/panels', loadChildren: () => import('./demo/view/panelsdemo.module').then(m => m.PanelsDemoModule) },
    { path: 'components/overlays', loadChildren: () => import('./demo/view/overlaysdemo.module').then(m => m.OverlaysDemoModule) },
    { path: 'components/menus', loadChildren: () => import('./demo/view/menusdemo.module').then(m => m.MenusDemoModule) },
    { path: 'components/messages', loadChildren: () => import('./demo/view/messagesdemo.module').then(m => m.MessagesDemoModule) },
    { path: 'components/misc', loadChildren: () => import('./demo/view/miscdemo.module').then(m => m.MiscDemoModule) },
    { path: 'pages/empty', loadChildren: () => import('./demo/view/emptydemo.module').then(m => m.EmptyDemoModule) },
    { path: 'components/charts', loadChildren: () => import('./demo/view/chartsdemo.module').then(m => m.ChartsDemoModule) },
    { path: 'components/file', loadChildren: () => import('./demo/view/filedemo.module').then(m => m.FileDemoModule) },
    { path: 'documentation', loadChildren: () => import('./demo/view/documentation.module').then(m => m.DocumentationModule) }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'});
