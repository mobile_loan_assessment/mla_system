import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocumentationComponent } from './documentation.component';
import { PrimeModule } from 'src/app/prime/prime.module';

const routes: Routes = [
  {
    path: '',
    component: DocumentationComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes), PrimeModule],
  declarations: [DocumentationComponent]
})
export class DocumentationModule { }
