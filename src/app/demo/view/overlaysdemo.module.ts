import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverlaysDemoComponent } from './overlaysdemo.component';
import { PrimeModule } from 'src/app/prime/prime.module';

const routes: Routes = [
  {
    path: '',
    component: OverlaysDemoComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes), PrimeModule],
  declarations: [OverlaysDemoComponent]
})
export class OverlaysDemoModule { }
