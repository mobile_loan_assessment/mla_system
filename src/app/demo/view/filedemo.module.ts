import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FileDemoComponent } from './filedemo.component';
import { PrimeModule } from 'src/app/prime/prime.module';

const routes: Routes = [
  {
    path: '',
    component: FileDemoComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes), PrimeModule],
  declarations: [FileDemoComponent]
})
export class FileDemoModule { }
