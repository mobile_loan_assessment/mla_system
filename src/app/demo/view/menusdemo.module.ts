import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenusDemoComponent } from './menusdemo.component';
import { PrimeModule } from 'src/app/prime/prime.module';

const routes: Routes = [
  {
    path: '',
    component: MenusDemoComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes), PrimeModule],
  declarations: [MenusDemoComponent]
})
export class MenusDemoModule { }
