import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PanelsDemoComponent } from './panelsdemo.component';
import { PrimeModule } from 'src/app/prime/prime.module';

const routes: Routes = [
  {
    path: '',
    component: PanelsDemoComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes), PrimeModule],
  declarations: [PanelsDemoComponent]
})
export class PanelsDemoModule { }
