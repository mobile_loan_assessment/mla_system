import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MiscDemoComponent } from './miscdemo.component';
import { PrimeModule } from 'src/app/prime/prime.module';

const routes: Routes = [
  {
    path: '',
    component: MiscDemoComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes), PrimeModule],
  declarations: [MiscDemoComponent]
})
export class MiscDemoModule { }
