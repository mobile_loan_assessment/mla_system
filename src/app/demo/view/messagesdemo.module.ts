import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessagesDemoComponent } from './messagesdemo.component';
import { PrimeModule } from 'src/app/prime/prime.module';

const routes: Routes = [
  {
    path: '',
    component: MessagesDemoComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes), PrimeModule],
  declarations: [MessagesDemoComponent]
})
export class MessagesDemoModule { }
