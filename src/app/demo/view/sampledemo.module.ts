import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SampleDemoComponent } from './sampledemo.component';
import { PrimeModule } from 'src/app/prime/prime.module';

const routes: Routes = [
  {
    path: '',
    component: SampleDemoComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes), PrimeModule],
  declarations: [SampleDemoComponent]
})
export class SampleDemoModule { }
