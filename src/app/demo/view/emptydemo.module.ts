import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmptyDemoComponent } from './emptydemo.component';
import { PrimeModule } from 'src/app/prime/prime.module';

const routes: Routes = [
  {
    path: '',
    component: EmptyDemoComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes), PrimeModule],
  declarations: [EmptyDemoComponent]
})
export class EmptyDemoModule { }
