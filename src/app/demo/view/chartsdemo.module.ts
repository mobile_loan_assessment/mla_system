import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartsDemoComponent } from './chartsdemo.component';
import { PrimeModule } from 'src/app/prime/prime.module';

const routes: Routes = [
  {
    path: '',
    component: ChartsDemoComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes), PrimeModule],
  declarations: [ChartsDemoComponent]
})
export class ChartsDemoModule { }
